    var ssi;
    var s = 0;

    function loadConfig() {
        $.ajax({
            url: ServerUrl+"gets3FilesMobile",
            type: "GET",
            data: {},
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function(data) {
              
                var bodyArray = (data);
                if ($("#copyrts").length) {
                    $("#copyrts").html(bodyArray.copyRight);
                };
                if ($("#trms").length) {
                    $("#trms").html('<a href="#" onClick=javascript:loadPage("' + bodyArray.termsofUse + '")>Terms of Use</a>');
                };
                if ($("#priv").length) {
                    $("#priv").html('<a href="#" onClick=javascript:loadPage("' + bodyArray.privacyPolicy + '")>Privacy</a>');
                };
                if ($("#supp").length) {
                    $("#supp").html('<a href="#" onClick=javascript:loadPage("' + bodyArray.support + '")>Support</a>');
                };
                $("#sourceURL").val(bodyArray.sourceurl);
               
                if ($(window).width() < 400) {
                    ssi = bodyArray.mobileSlideShowProtrait;
                } else {
                    ssi = bodyArray.mobileSlideShowLanscape;
                }

                if ($("#bxslider").length) {
                    slideShow();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //alert("Retrive config error.");
            },
            complete: function() {

            }
        });

    }





    function slideShow() {
        var imgLen = ssi.length;
        var bxslider = '';
        $.each(ssi, function(key, value) {
            bxslider = bxslider + '<li><img src="' + $("#sourceURL").val() + '/' + value + '" title="" /></li>';
            if (key + 1 == imgLen) {
                $("#bxslider").html(bxslider);
                $('.bxslider').bxSlider({
                    auto: true,
                    mode: 'horizontal',
                    controls: false,
                    pager: false,
                    touchEnabled: true,
                    speed: 300,
                    easing: 'ease-out'
                });
            }
        });
    }
